var formDangNhap = {
    init: function () {
        this.dangNhap = document.getElementById("dangNhap");
        this.bgDangNhap = document.getElementById("bgDangNhap");
        this.khachHang = document.getElementById("khachHang");
        this.nhaCungCap = document.getElementById("nhaCungCap");
        this.formDemo = document.querySelector(".formDemo");
        this.menu2 = document.getElementById("menu2");
        this.menu2Left = document.querySelector(".menu2Left");
        this.iconUser = document.getElementById("iconUser");
        this.dangNhapForm();
    },
    dangNhapForm: function () {
        $("#iconBar").click(function () {
            $("#subMenu ul").slideToggle();
            $("#subMenu ul ul").css("display", "none");
        })
        $("#subMenu ul li").click(function () {
            $("#subMenu ul ul").slideUp();
            $(this).find('ul').slideToggle();
        });
        $("#iconUser").click(function () {
            $("#infoUser").slideToggle();
        })
        this.dangNhap.onclick = function () {
            $("#bgDangNhap").addClass("addBlock");
        }
        window.onclick = function () {
            if (event.target == this.bgDangNhap) {
                $("#bgDangNhap").removeClass("addBlock");
            }
        }

        this.nhaCungCap.onclick = function () {
            $(".formDemo").addClass("addNhaCungCap");
            $("#khachHang").addClass("addBg");
            $("#khachHang").removeClass("addBorder");
            $("#nhaCungCap").addClass("addBorder");
            $("#nhaCungCap").removeClass("addBg");
            $(".formDemo").removeClass("addKhachHang");
        }
        this.khachHang.onclick = function () {
            $(".formDemo").removeClass("addNhaCungCap");
            $("#khachHang").removeClass("addBg");
            $("#khachHang").addClass("addBorder");
            $("#nhaCungCap").removeClass("addBorder");
            $("#nhaCungCap").addClass("addBg");
            $(".formDemo").addClass("addKhachHang");
        }
        this.iconUser.onclick = function () {
            $('.menu2Right').toggleClass('addRightmenu');
            // alert("hello");
        }
        window.addEventListener('scroll', function () {
            if (this.pageYOffset > 200) {
                this.menu2.classList.add("addMenu2");
            }
            else {
                this.menu2.classList.remove("addMenu2");
            }

        });
    }
}
formDangNhap.init();