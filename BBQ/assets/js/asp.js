var header = {
    init: function(){
        this.iconBar = document.querySelector(".iconBar");       
        this.formMenu = document.querySelector(".formMenu");
        this.iconClose = document.querySelector(".iconClose");
        this.header = document.querySelector(".header");
        this.iconSearch = document.querySelector(".iconSearch i");
        this.menuHeader();
        this.checkedItems();
    },
    menuHeader: function(){
        this.iconSearch.onclick = function(){
            $('.tooltipSearch').toggleClass('addTooltipSearch');
        }
        this.iconBar.onclick = function(){
            $('.bgFormMenu').addClass('showMenu');
        }
        this.iconClose.onclick = function(){
            $('.bgFormMenu').removeClass('showMenu');
        }
        window.onclick = function () {
            bgFormMenu = document.querySelector(".bgFormMenu");
            if (event.target == bgFormMenu) {
                // alert("hello");
                $(".bgFormMenu").removeClass("showMenu");
                
            }
        }
        window.addEventListener('scroll', function () {
            if (this.pageYOffset > 250) {
                $('#headerTong').addClass("fixedMenu");
            }
            else {
                $('#headerTong').removeClass("fixedMenu");
            }
            
        });
        
    },
    checkedItems: function(){
        $('input[type="checkbox"]').click(function() {
            $('input[type="checkbox"]').not(this).prop('checked', false);
        });
    }
}
header.init();
