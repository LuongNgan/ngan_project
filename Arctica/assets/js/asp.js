var About = {
    init: function () {
        this.header();
        this.slider();   
    },
    header: function () {
        //Xu ly thay doi icon bar
        $('.topBarIconBar').click(function () {
            $('.replaceMenu').slideToggle();
            $(this).toggleClass('changeIconBar');
        })

        window.addEventListener('scroll', (function (event) {
            this.scrollTop = window.pageYOffset;
            this.header = document.querySelector('.header');
            if (scrollTop > 200) {
                header.classList.add("fixed");
            } else {
                header.classList.remove("fixed");
            }
        }))

    },
    slider: function () {
        var $owl = $('#owl-2.owl-carousel');

        $owl.children().each(function (index) {
            $(this).attr('data-position', index);
        });

        $owl.owlCarousel({

            center: true,
            loop: true,
            dots: true,
            items: 1,
            autoplay: true,
            autoplayTimeout: 3000,
        });

        $(document).on('click', '.owl-item>div', function () {
            $owl.trigger('to.owl.carousel', $(this).data('position'));
        });
    }
}
About.init();
var Home = {
    init: function () {
        this.sliderSec2();
        this.sliderHome();
    },
    sliderSec2: function () {
        $(document).ready(function () {

            $("#owl-Section2").owlCarousel({

                navigation: true, // Show next and prev buttons
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true,
                nav: true,
                dots:false,

                // "singleItem:true" is a shortcut for:
                items : 1, 
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

        });
    },
    sliderHome: function(){
        $("#owl-sec6").owlCarousel({
           
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            dots:true,
            margin:10,
       
            // "singleItem:true" is a shortcut for:
            items : 1, 
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false
       
        });
    }
}
Home.init();
var Shop = {
    init: function(){
        this.tab();
    },
    tab:function(){
        $(document).ready(function() {
            $("ul.tabs li").click(function() {
              var tab_id = $(this).attr("data-tab");
    
              $("ul.tabs li").removeClass("current");
              $(".tab-content").removeClass("current");
    
              $(this).addClass("current");
              $("#" + tab_id).addClass("current");
            });
          });
    }
}
Shop.init();
